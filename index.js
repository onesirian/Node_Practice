const _ = require('lodash');
const express = require('express');
const app = express();
var v = require("./variables.js");
console.log("node running");
var array = [1];
var other = _.concat(array, 2, [3], [[4]]);
console.log(other);
console.log(v);
app.use(express.static('static'));
app.get('/', (req, res) => {
    res.send('<h1>Hi! This is my first string carried by Express. Oh yeah!</h1>')
})
app.get('/about', (req, res) => {
    res.send({ page: "About Page" })
})
app.get('/gallery', (req, res) => {
    res.send('<h1>Gallery!</h1><div><img src="img/starwars.jpg" /></div>');
})
app.listen(3000, () => {
    console.log('Server is running on http://www.localhost:3000! To stop server, press Control C' )
})